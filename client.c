#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#define SA struct sockaddr

/* chiede al server l'ora, ritorna 0 se tutto è andato a buon fine */
int askToServer()
{
	//dichiarazioni delle variabili
	struct sockaddr_in srv_addr;
	int ret, sk;
	char *request = "GET_TIME";
	char msg[1024];
	char ip[20];
	int porta;
	
	//inserimento indirizzo IP e porta
	printf("Inserisci indirizzo IP e porta: ");
	scanf("%s %i",ip,&porta);
	
	//creazione del socket TCP
	sk = socket(AF_INET, SOCK_STREAM, 0);
	
	//azzeramento della struttura
	memset(&srv_addr, 0, sizeof(srv_addr));
	
	//inserimento nella struttura dell'Ip e porta
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(porta);
	ret = inet_pton(AF_INET,ip, &srv_addr.sin_addr);
	
	//connessione al server
	ret = connect(sk, (SA *) &srv_addr, sizeof(srv_addr));
	
	//controllo connessione
	if(ret == -1)
	{
		perror("\nErrore di connessione");
		return -1;
	}
	
	//invio richiesta
	ret = send(sk, (void*) request, strlen(request),0);
	
	//controllo invio richiesta
	if(ret == -1 || ret < strlen(request))
	{
		perror("\nErrore di invio dati");
		return -1;
	}
		
	//ricezione messaggio
	ret = recv(sk, (void *) msg, 26, MSG_WAITALL);
	
	if(ret == -1)
	{
		perror("\nErrore ricezione pacchetto");
		return -1;
	}
	
	//stampa messaggio e chiusura socket
	printf("\nRisposta del server: %s\n",msg);
	close(sk);
	
	return 0;
}

int main(null)
{
	int result;
	char stop = 'y';
	
	do{
		//richiesta dell'ora al server
		printf("\n\nRichiesta dell'ora al server ...\n");
		result = askToServer();
		
		//prompt
		printf("Richiedere di nuovo l'ora al server? (Y,n): ");
		scanf("\n%c",&stop);
		
	} while(stop != 'n');
	
	printf("\nChiusura client in corso...\n");
	return 0;
}
