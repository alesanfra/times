#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#define SA struct sockaddr

int answerToClient(int sk)
{
	int ret, len = 9;
	char request[10], msg[26];
	time_t now = time(NULL);
	
	//azzeramento di request
	memset((void*) request, ' ',10); 
	
	//creazione stringa con data
	ctime_r(&now,msg);
	
	//ricezione della richiesta dal client
	ret = recv(sk, (void *) request, len,0);
	
	//Stampa della richiesta
	printf("\nRichiesta del client: %s",request);
	
	if(ret == -1)
	{
		perror("\nErrore ricezione pacchetto");
		return -1;
	}
	
	//invio risposta
	ret = send(sk, (void*) msg, strlen(msg),0);
	
	//controllo invio richiesta
	if(ret == -1 || ret < strlen(msg))
	{
		perror("\nErrore di invio dati");
		return -1;
	}
	
	//Stampa informazioni e chiusura connessione	
	printf("\nDati inviati al client con successo\n");
	close(sk);
	
	return 0;
}

int main(null)
{
	//allocazione delle strutture dati necessarie
	struct sockaddr_in my_addr, cl_addr;
	int ret, len, sk, cn_sk;
	
	//Verbose
	printf("Avvio del server\n");
	
	//creazione del socket di ascolto
	sk = socket(AF_INET, SOCK_STREAM, 0);
	
	if(sk == -1)
	{
		perror("Errore nella creazione del socket");
		return 0;
	}
	else
		printf("Socket di ascolto creato correttamente\n");
	
	//inizializzazione delle strutture dati
	memset(&my_addr, 0, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	my_addr.sin_port = htons(1234);
	
	//bind del socket
	ret = bind(sk, (SA *) &my_addr, sizeof(my_addr));
	
	if(ret == -1)
	{
		perror("Errore nell'esecuzione della bind del socket");
		return 0;
	}
	else
		printf("Bind del socket eseguita correttamente\n");
		
	//messa in ascolto del server sul socket
	ret = listen(sk, 10);
	
	if(ret == -1)
	{
		perror("Errore nella messa in ascolto del server");
		return 0;
	}
	else
		printf("Server messo in ascolto sul socket correttamente\n");
	
	//dimensioni della struttura dove viene salvato l'ind del client
	len = sizeof(cl_addr);
	
	//ciclo in cui il server accetta connessioni in ingresso e le gestisce
	for(;;)
	{
		//accettazione delle connessioni ingresso
		cn_sk = accept(sk, (SA *) &cl_addr, (socklen_t *) &len);
		
		if(cn_sk == -1)
		{
			perror("Errore nell'accettazione di una richiesta");
			return 0;
		}
		else
			printf("Richiesta accettata correttamente\n");
		
		//gestione delle richieste
		answerToClient(cn_sk);
	}
	
	//Chiusura del socket di ascolto
	//Questo codice non verrà mai eseguito dato che si trova
	//dopo un ciclo infinito
	
	printf("\nChiusura del server\n");
	close(sk);
	return 0;	
}
